
#import <GLKit/GLKit.h>
#import <AVFoundation/AVFoundation.h>
#import "MakeupArtist.h"

@interface MakeupViewController : GLKViewController 

- (void)flipCamera:(void (^)(void))completion;

- (UIImage *)captureImage;

- (void)startRecording:(NSURL *)movieURL;
- (void)finishRecordingWithCompletionHandler:(void (^)(void))handler;
- (void)cancelRecording;

@property (strong, nonatomic) MakeupArtist  *artist;

@end

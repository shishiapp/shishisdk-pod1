//
//  ShiShiSdk.h
//  ShiShiSdk
//
//  Created by Lingyu Wang on 2018-05-04.
//  Copyright © 2018 ShiShi. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ShiShiSdk.
FOUNDATION_EXPORT double ShiShiSdkVersionNumber;

//! Project version string for ShiShiSdk.
FOUNDATION_EXPORT const unsigned char ShiShiSdkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ShiShiSdk/PublicHeader.h>

#import <ShiShiSdk/MakeupArtist.h>
#import <ShiShiSdk/MakeupViewController.h>
